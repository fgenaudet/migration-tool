package com.fgenaudet.batchmigration.target.dao;

import com.fgenaudet.batchmigration.target.models.Person;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TargetPersonRepository extends JpaRepository<Person, Long> {
    Person findByFirstNameAndLastName(String firstName, String lastName);
}
