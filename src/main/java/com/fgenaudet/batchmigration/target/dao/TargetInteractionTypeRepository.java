package com.fgenaudet.batchmigration.target.dao;

import com.fgenaudet.batchmigration.target.models.InteractionType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TargetInteractionTypeRepository extends JpaRepository<InteractionType, Long> {
    InteractionType findByCode(String code);
}
