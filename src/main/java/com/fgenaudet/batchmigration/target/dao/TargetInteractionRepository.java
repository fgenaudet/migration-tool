package com.fgenaudet.batchmigration.target.dao;

import com.fgenaudet.batchmigration.target.models.Interaction;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TargetInteractionRepository extends JpaRepository<Interaction, Long> {
}
