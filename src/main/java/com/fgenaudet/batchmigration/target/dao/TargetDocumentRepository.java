package com.fgenaudet.batchmigration.target.dao;

import com.fgenaudet.batchmigration.target.models.Document;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TargetDocumentRepository extends JpaRepository<Document, Long> {
}
