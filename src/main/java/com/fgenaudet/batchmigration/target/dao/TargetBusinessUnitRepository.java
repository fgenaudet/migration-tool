package com.fgenaudet.batchmigration.target.dao;

import com.fgenaudet.batchmigration.target.models.BusinessUnit;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TargetBusinessUnitRepository extends JpaRepository<BusinessUnit, Long> {
}
