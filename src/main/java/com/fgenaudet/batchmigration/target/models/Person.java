package com.fgenaudet.batchmigration.target.models;

import javax.persistence.*;
import java.util.List;

@Entity
public class Person {
    @Id
    public Long id;

    public String firstName;
    public String lastName;

    @OneToMany(mappedBy = "author", cascade = CascadeType.ALL)
    public List<Interaction> personInteraction;
}
