package com.fgenaudet.batchmigration.target.models;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

@Entity
public class Interaction {
    @Id
    public Long id;

    public String name;

    @Column(name = "creationDate", columnDefinition = "DATE")
    public Date creationDate;

    @ManyToOne
    public InteractionType type;

    @ManyToOne
    public Person author;

    @ManyToMany
    public List<Document> documents;

    @ManyToOne
    public BusinessUnit businessUnit;
}
