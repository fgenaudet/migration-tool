package com.fgenaudet.batchmigration.target.models;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import java.time.LocalDate;

@Entity
public class Document {
    @Id
    public Long id;

    public String name;
    public LocalDate creationDate;
    public String content;

    @ManyToOne
    public BusinessUnit businessUnit;
}
