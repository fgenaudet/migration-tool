package com.fgenaudet.batchmigration.target.models;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class BusinessUnit {
    @Id
    public Long id;

    public String code;
    public String name;
}
