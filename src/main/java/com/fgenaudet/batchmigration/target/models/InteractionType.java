package com.fgenaudet.batchmigration.target.models;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

@Entity
public class InteractionType {
    @Id
    public Long id;

    public String code;
    public String name;

    @ManyToOne
    public BusinessUnit businessUnit;
}
