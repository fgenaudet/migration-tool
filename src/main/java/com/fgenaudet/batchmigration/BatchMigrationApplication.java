package com.fgenaudet.batchmigration;

import com.fgenaudet.batchmigration.jobs.JobScheduler;
import com.fgenaudet.batchmigration.tests.EnsureMigrationConsistency;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;

@SpringBootApplication
public class BatchMigrationApplication {

    public static void main(String[] args) throws Exception {
        ConfigurableApplicationContext context = SpringApplication.run(BatchMigrationApplication.class, args);

        // Init Data First
        InitializeData initializeData = context.getBean("initializeData", InitializeData.class);
        initializeData.run(null);

        // Then run migration
        JobScheduler jobScheduler = context.getBean("jobScheduler", JobScheduler.class);
        jobScheduler.run();

        // Finally ensure migration is ok
        EnsureMigrationConsistency ensureMigrationConsistency = context.getBean("ensureMigrationConsistency", EnsureMigrationConsistency.class);
        ensureMigrationConsistency.run();
    }

}
