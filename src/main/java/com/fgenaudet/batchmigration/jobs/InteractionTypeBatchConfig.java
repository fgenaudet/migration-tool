package com.fgenaudet.batchmigration.jobs;

import com.fgenaudet.batchmigration.processors.InteractionTypeProcessor;
import com.fgenaudet.batchmigration.source.dao.SourceInteractionTypeRepository;
import com.fgenaudet.batchmigration.source.models.InteractionType;
import com.fgenaudet.batchmigration.target.dao.TargetInteractionTypeRepository;
import org.springframework.batch.core.Job;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.JpaRepository;

@Configuration
public class InteractionTypeBatchConfig extends GenericBatchJob<InteractionType, com.fgenaudet.batchmigration.target.models.InteractionType> {
    @Autowired
    public SourceInteractionTypeRepository sourceInteractionTypeRepository;

    @Autowired
    public TargetInteractionTypeRepository targetInteractionTypeRepository;

    @Autowired
    public InteractionTypeProcessor entityProcessor;

    @Override
    public ItemProcessor<InteractionType, com.fgenaudet.batchmigration.target.models.InteractionType> processor() {
        return entityProcessor;
    }

    @Override
    public JpaRepository<InteractionType, Long> sourceRepository() {
        return sourceInteractionTypeRepository;
    }

    @Override
    public JpaRepository<com.fgenaudet.batchmigration.target.models.InteractionType, Long> targetRepository() {
        return targetInteractionTypeRepository;
    }

    @Override
    public String name() {
        return "migrateInteractionType";
    }

    @Bean
    public Job migrateInteractionType() {
        return super.migrateData();
    }
}
