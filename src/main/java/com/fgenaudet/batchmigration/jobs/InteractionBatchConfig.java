package com.fgenaudet.batchmigration.jobs;

import com.fgenaudet.batchmigration.processors.DocumentProcessor;
import com.fgenaudet.batchmigration.processors.InteractionProcessor;
import com.fgenaudet.batchmigration.source.dao.SourceDocumentRepository;
import com.fgenaudet.batchmigration.source.dao.SourceInteractionRepository;
import com.fgenaudet.batchmigration.source.models.Document;
import com.fgenaudet.batchmigration.source.models.Interaction;
import com.fgenaudet.batchmigration.target.dao.TargetDocumentRepository;
import com.fgenaudet.batchmigration.target.dao.TargetInteractionRepository;
import com.fgenaudet.batchmigration.target.models.BusinessUnit;
import org.springframework.batch.core.Job;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.JpaRepository;

@Configuration
public class InteractionBatchConfig extends GenericBatchJob<Interaction, com.fgenaudet.batchmigration.target.models.Interaction> {
    @Autowired
    public SourceInteractionRepository sourceRepository;

    @Autowired
    public TargetInteractionRepository targetRepository;

    @Autowired
    public InteractionProcessor entityProcessor;

    @Override
    public ItemProcessor<Interaction, com.fgenaudet.batchmigration.target.models.Interaction> processor() {
        return entityProcessor;
    }

    @Override
    public JpaRepository<Interaction, Long> sourceRepository() {
        return sourceRepository;
    }

    @Override
    public JpaRepository<com.fgenaudet.batchmigration.target.models.Interaction, Long> targetRepository() {
        return targetRepository;
    }

    @Override
    public String name() {
        return "migrateInteraction";
    }

    @Bean
    public Job migrateInteraction() {
        return super.migrateData();
    }
}
