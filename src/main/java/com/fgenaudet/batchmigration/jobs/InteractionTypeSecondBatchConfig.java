package com.fgenaudet.batchmigration.jobs;

import com.fgenaudet.batchmigration.processors.InteractionTypeSecondProcessor;
import com.fgenaudet.batchmigration.secondsource.dao.SecondSourceInteractionTypeRepository;
import com.fgenaudet.batchmigration.secondsource.models.InteractionType;
import com.fgenaudet.batchmigration.target.dao.TargetInteractionTypeRepository;
import org.springframework.batch.core.Job;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.JpaRepository;

@Configuration
public class InteractionTypeSecondBatchConfig extends GenericBatchJob<InteractionType, com.fgenaudet.batchmigration.target.models.InteractionType> {
    @Autowired
    public SecondSourceInteractionTypeRepository sourceRepository;

    @Autowired
    public TargetInteractionTypeRepository targetRepository;

    @Autowired
    public InteractionTypeSecondProcessor entityProcessor;

    @Override
    public ItemProcessor<InteractionType, com.fgenaudet.batchmigration.target.models.InteractionType> processor() {
        return entityProcessor;
    }

    @Override
    public JpaRepository<InteractionType, Long> sourceRepository() {
        return sourceRepository;
    }

    @Override
    public JpaRepository<com.fgenaudet.batchmigration.target.models.InteractionType, Long> targetRepository() {
        return targetRepository;
    }

    @Override
    public String name() {
        return "migrateInteractionTypeSecond";
    }

    @Bean
    public Job migrateInteractionTypeSecond() {
        return super.migrateData();
    }
}
