package com.fgenaudet.batchmigration.jobs;

import com.fgenaudet.batchmigration.processors.DocumentProcessor;
import com.fgenaudet.batchmigration.processors.PersonProcessor;
import com.fgenaudet.batchmigration.source.dao.SourceDocumentRepository;
import com.fgenaudet.batchmigration.source.dao.SourcePersonRepository;
import com.fgenaudet.batchmigration.source.models.Document;
import com.fgenaudet.batchmigration.source.models.Person;
import com.fgenaudet.batchmigration.target.dao.TargetDocumentRepository;
import com.fgenaudet.batchmigration.target.dao.TargetPersonRepository;
import org.springframework.batch.core.Job;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.JpaRepository;

@Configuration
public class PersonBatchConfig extends GenericBatchJob<Person, com.fgenaudet.batchmigration.target.models.Person> {
    @Autowired
    public SourcePersonRepository sourceRepository;

    @Autowired
    public TargetPersonRepository targetRepository;

    @Autowired
    private PersonProcessor entityProcessor;

    @Override
    public ItemProcessor<Person, com.fgenaudet.batchmigration.target.models.Person> processor() {
        return entityProcessor;
    }

    @Override
    public JpaRepository<Person, Long> sourceRepository() {
        return sourceRepository;
    }

    @Override
    public JpaRepository<com.fgenaudet.batchmigration.target.models.Person, Long> targetRepository() {
        return targetRepository;
    }

    @Override
    public String name() {
        return "migratePerson";
    }

    @Bean
    public Job migratePerson() {
        return super.migrateData();
    }
}
