package com.fgenaudet.batchmigration.jobs;

import com.fgenaudet.batchmigration.target.dao.TargetBusinessUnitRepository;
import com.fgenaudet.batchmigration.target.models.BusinessUnit;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.launch.support.RunIdIncrementer;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.batch.item.ItemReader;
import org.springframework.batch.item.ItemWriter;
import org.springframework.batch.item.data.RepositoryItemReader;
import org.springframework.batch.item.data.RepositoryItemWriter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.HashMap;
import java.util.Map;

public abstract class GenericBatchJob<S, T> {
    @Autowired
    public JobBuilderFactory jobBuilderFactory;

    @Autowired
    public StepBuilderFactory stepBuilderFactory;

    @Autowired
    public Environment environment;

    @Autowired
    public TargetBusinessUnitRepository targetBusinessUnitRepository;

    public ItemReader<S> reader() {
        RepositoryItemReader<S> itemReader = new RepositoryItemReader<>();
        itemReader.setMethodName("findAll");
        itemReader.setRepository(sourceRepository());
        Map<String, Sort.Direction> sorts = new HashMap<>();
        sorts.put("id", Sort.Direction.ASC);
        itemReader.setSort(sorts);
        itemReader.setPageSize(10000);

        return itemReader;
    }

    public ItemProcessor<S, T> itemProcessor() {
        return processor();
    }

    public ItemWriter<T> writer() {
        RepositoryItemWriter<T> repositoryItemWriter = new RepositoryItemWriter<>();
        repositoryItemWriter.setMethodName("save");
        repositoryItemWriter.setRepository(targetRepository());

        return repositoryItemWriter;
    }


    public Job migrateData() {
        return jobBuilderFactory.get(name())
                .incrementer(new RunIdIncrementer())
                .flow(step())
                .end()
                .build();
    }

    public Step step() {
        return stepBuilderFactory.get("step1")
                .<S, T>chunk(10)
                .reader(reader())
                .processor(itemProcessor())
                .writer(writer())
                .build();
    }

    public abstract ItemProcessor<S, T> processor();

    public abstract JpaRepository<S, Long> sourceRepository();

    public abstract JpaRepository<T, Long> targetRepository();

    public abstract String name();


}
