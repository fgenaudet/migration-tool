package com.fgenaudet.batchmigration.jobs;

import com.fgenaudet.batchmigration.processors.DocumentSecondProcessor;
import com.fgenaudet.batchmigration.secondsource.dao.SecondSourceDocumentRepository;
import com.fgenaudet.batchmigration.secondsource.models.Document;
import com.fgenaudet.batchmigration.target.dao.TargetDocumentRepository;
import org.springframework.batch.core.Job;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.JpaRepository;

@Configuration
public class DocumentSecondBatchConfig extends GenericBatchJob<Document, com.fgenaudet.batchmigration.target.models.Document> {
    @Autowired
    public SecondSourceDocumentRepository sourceRepository;

    @Autowired
    public TargetDocumentRepository targetRepository;

    @Autowired
    private DocumentSecondProcessor entityProcessor;

    @Override
    public ItemProcessor<Document, com.fgenaudet.batchmigration.target.models.Document> processor() {
        return entityProcessor;
    }

    @Override
    public JpaRepository<Document, Long> sourceRepository() {
        return sourceRepository;
    }

    @Override
    public JpaRepository<com.fgenaudet.batchmigration.target.models.Document, Long> targetRepository() {
        return targetRepository;
    }

    @Override
    public String name() {
        return "migrateDocumentSecond";
    }

    @Bean
    public Job migrateDocumentSecond() {
        return super.migrateData();
    }
}
