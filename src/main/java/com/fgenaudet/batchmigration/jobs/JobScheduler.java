package com.fgenaudet.batchmigration.jobs;

import com.fgenaudet.batchmigration.target.dao.TargetBusinessUnitRepository;
import com.fgenaudet.batchmigration.target.models.BusinessUnit;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.JobParameter;
import org.springframework.batch.core.JobParameters;
import org.springframework.batch.core.job.builder.JobBuilder;
import org.springframework.batch.core.launch.JobLauncher;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.boot.env.YamlPropertySourceLoader;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.ClassPathScanningCandidateComponentProvider;
import org.springframework.core.env.Environment;
import org.springframework.core.env.PropertySource;
import org.springframework.core.type.filter.AnnotationTypeFilter;
import org.springframework.data.util.AnnotatedTypeScanner;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Component
public class JobScheduler {
    @Autowired
    ConfigurableApplicationContext context;

    @Autowired
    JobLauncher jobLauncher;

    public void run() throws Exception {
        List<PropertySource<?>> propertySources = new YamlPropertySourceLoader().load("jobs.yml", context.getResource("jobs.yml"));
        List<String> jobsOrder = (List<String>) ((Map)propertySources.get(0)
                .getSource())
                .values()
                .stream().map(x -> x.toString()).collect(Collectors.toList());

        Map<String, Job> jobs =  context.getBeanFactory().getBeansOfType(Job.class);
        Map<String, Job> jobsByName = jobs.values().stream().collect(Collectors.toMap(x -> x.getName(), x -> x));



        for (String job: jobsOrder) {
            if (jobsByName.containsKey(job)) {
                System.out.println("Starting " + job + "=======================");
                jobLauncher.run(jobsByName.get(job), new JobParameters());
            }
        }
    }
}
