package com.fgenaudet.batchmigration.jobs;

import com.fgenaudet.batchmigration.processors.DocumentProcessor;
import com.fgenaudet.batchmigration.source.dao.SourceDocumentRepository;
import com.fgenaudet.batchmigration.source.models.Document;
import com.fgenaudet.batchmigration.target.dao.TargetDocumentRepository;
import com.fgenaudet.batchmigration.target.models.BusinessUnit;
import org.springframework.batch.core.Job;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.JpaRepository;

@Configuration
public class DocumentBatchConfig extends GenericBatchJob<Document, com.fgenaudet.batchmigration.target.models.Document> {
    @Autowired
    public SourceDocumentRepository sourceRepository;

    @Autowired
    public TargetDocumentRepository targetRepository;

    @Autowired
    private DocumentProcessor entityProcessor;

    @Override
    public ItemProcessor<Document, com.fgenaudet.batchmigration.target.models.Document> processor() {
        return entityProcessor;
    }

    @Override
    public JpaRepository<Document, Long> sourceRepository() {
        return sourceRepository;
    }

    @Override
    public JpaRepository<com.fgenaudet.batchmigration.target.models.Document, Long> targetRepository() {
        return targetRepository;
    }

    @Override
    public String name() {
        return "migrateDocument";
    }

    @Bean
    public Job migrateDocument() {
        return super.migrateData();
    }
}
