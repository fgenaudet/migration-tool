package com.fgenaudet.batchmigration.jobs;

import com.fgenaudet.batchmigration.processors.InteractionSecondProcessor;
import com.fgenaudet.batchmigration.secondsource.dao.SecondSourceInteractionRepository;
import com.fgenaudet.batchmigration.secondsource.models.Interaction;
import com.fgenaudet.batchmigration.target.dao.TargetInteractionRepository;
import org.springframework.batch.core.Job;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.JpaRepository;

@Configuration
public class InteractionSecondBatchConfig extends GenericBatchJob<Interaction, com.fgenaudet.batchmigration.target.models.Interaction> {
    @Autowired
    public SecondSourceInteractionRepository sourceRepository;

    @Autowired
    public TargetInteractionRepository targetRepository;

    @Autowired
    public InteractionSecondProcessor entityProcessor;

    @Override
    public ItemProcessor<Interaction, com.fgenaudet.batchmigration.target.models.Interaction> processor() {
        return entityProcessor;
    }

    @Override
    public JpaRepository<Interaction, Long> sourceRepository() {
        return sourceRepository;
    }

    @Override
    public JpaRepository<com.fgenaudet.batchmigration.target.models.Interaction, Long> targetRepository() {
        return targetRepository;
    }

    @Override
    public String name() {
        return "migrateInteractionSecond";
    }

    @Bean
    public Job migrateInteractionSecond() {
        return super.migrateData();
    }
}
