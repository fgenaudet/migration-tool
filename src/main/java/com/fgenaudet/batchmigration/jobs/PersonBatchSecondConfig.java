package com.fgenaudet.batchmigration.jobs;

import com.fgenaudet.batchmigration.processors.PersonSecondProcessor;
import com.fgenaudet.batchmigration.secondsource.dao.SecondSourcePersonRepository;
import com.fgenaudet.batchmigration.secondsource.models.Person;
import com.fgenaudet.batchmigration.target.dao.TargetPersonRepository;
import org.springframework.batch.core.Job;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.JpaRepository;

@Configuration
public class PersonBatchSecondConfig extends GenericBatchJob<Person, com.fgenaudet.batchmigration.target.models.Person> {
    @Autowired
    public SecondSourcePersonRepository sourceRepository;

    @Autowired
    public TargetPersonRepository targetRepository;

    @Autowired
    private PersonSecondProcessor entityProcessor;

    @Override
    public ItemProcessor<Person, com.fgenaudet.batchmigration.target.models.Person> processor() {
        return entityProcessor;
    }

    @Override
    public JpaRepository<Person, Long> sourceRepository() {
        return sourceRepository;
    }

    @Override
    public JpaRepository<com.fgenaudet.batchmigration.target.models.Person, Long> targetRepository() {
        return targetRepository;
    }

    @Override
    public String name() {
        return "migrateSecondPerson";
    }

    @Bean
    public Job migrateSecondPerson() {
        return super.migrateData();
    }
}
