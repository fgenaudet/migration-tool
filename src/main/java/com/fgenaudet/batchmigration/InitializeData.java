package com.fgenaudet.batchmigration;

import com.fgenaudet.batchmigration.utils.InitSecondsourceData;
import com.fgenaudet.batchmigration.utils.InitTargetData;
import com.fgenaudet.batchmigration.utils.InitSourceData;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.stereotype.Component;

@Component
public class InitializeData {
    @Autowired
    private InitSourceData initSourceData;

    @Autowired
    private InitTargetData initTargetData;

    @Autowired
    private InitSecondsourceData initSecondsourceData;


    public void run(ApplicationArguments args) {
        initSourceData.run();
        initSecondsourceData.run();
        initTargetData.run();
    }
}
