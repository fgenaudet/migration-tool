package com.fgenaudet.batchmigration.secondsource.models;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class InteractionType {
    @Id
    public Long id;

    public String code;
    public String name;
}
