package com.fgenaudet.batchmigration.secondsource.models;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import java.util.List;

@Entity
public class Person {
    @Id
    public Long id;

    public String firstName;
    public String lastName;

    @OneToMany(fetch = FetchType.EAGER, mappedBy = "author")
    public List<Interaction> personInteraction;
}
