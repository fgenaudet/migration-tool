package com.fgenaudet.batchmigration.secondsource.dao;

import com.fgenaudet.batchmigration.secondsource.models.Interaction;
import org.springframework.data.jpa.repository.JpaRepository;

public interface SecondSourceInteractionRepository extends JpaRepository<Interaction, Long> {
}
