package com.fgenaudet.batchmigration.secondsource.dao;

import com.fgenaudet.batchmigration.secondsource.models.Person;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface SecondSourcePersonRepository extends JpaRepository<Person, Long> {
}
