package com.fgenaudet.batchmigration.secondsource.dao;

import com.fgenaudet.batchmigration.secondsource.models.InteractionType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface SecondSourceInteractionTypeRepository extends JpaRepository<InteractionType, Long> {
}
