package com.fgenaudet.batchmigration.secondsource.dao;

import com.fgenaudet.batchmigration.secondsource.models.Document;
import org.springframework.data.jpa.repository.JpaRepository;

public interface SecondSourceDocumentRepository extends JpaRepository<Document, Long> {
}
