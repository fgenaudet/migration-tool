package com.fgenaudet.batchmigration.processors;

import com.fgenaudet.batchmigration.source.models.Document;
import com.fgenaudet.batchmigration.target.dao.TargetBusinessUnitRepository;
import com.fgenaudet.batchmigration.target.models.BusinessUnit;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class DocumentProcessor implements ItemProcessor<Document, com.fgenaudet.batchmigration.target.models.Document> {

    @Autowired
    private TargetBusinessUnitRepository businessUnitRepository;

    @Value("#{@environment.getProperty('source.businessUnit.id')}")
    private Long businessUnitId;

    @Value("#{@environment.getProperty('source.offset')}")
    private Integer sourceOffset;

    @Override
    public com.fgenaudet.batchmigration.target.models.Document process(Document document) throws Exception {
        BusinessUnit businessUnit = businessUnitRepository.findById(businessUnitId).orElse(null);

        com.fgenaudet.batchmigration.target.models.Document targetDocument = new com.fgenaudet.batchmigration.target.models.Document();
        targetDocument.id = sourceOffset + document.id;
        targetDocument.content = document.content;
        targetDocument.creationDate = document.creationDate;
        targetDocument.name = document.name;
        targetDocument.businessUnit = businessUnit;

        return targetDocument;
    }
}
