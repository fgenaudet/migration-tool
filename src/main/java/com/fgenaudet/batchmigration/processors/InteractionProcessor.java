package com.fgenaudet.batchmigration.processors;

import com.fgenaudet.batchmigration.source.models.Interaction;
import com.fgenaudet.batchmigration.target.dao.TargetBusinessUnitRepository;
import com.fgenaudet.batchmigration.target.dao.TargetDocumentRepository;
import com.fgenaudet.batchmigration.target.dao.TargetInteractionTypeRepository;
import com.fgenaudet.batchmigration.target.models.BusinessUnit;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.util.stream.Collectors;

@Component
public class InteractionProcessor implements ItemProcessor<Interaction, com.fgenaudet.batchmigration.target.models.Interaction> {

    @Autowired
    private TargetInteractionTypeRepository interactionTypeRepository;

    @Autowired
    private TargetDocumentRepository documentRepository;

    @Autowired
    private TargetBusinessUnitRepository businessUnitRepository;

    @Value("#{@environment.getProperty('source.businessUnit.id')}")
    private Long businessUnitId;

    @Value("#{@environment.getProperty('source.offset')}")
    private Integer sourceOffset;

    @Override
    public com.fgenaudet.batchmigration.target.models.Interaction process(Interaction interaction) throws Exception {
        BusinessUnit businessUnit = businessUnitRepository.findById(businessUnitId).orElse(null);

        com.fgenaudet.batchmigration.target.models.Interaction targetInteraction = new com.fgenaudet.batchmigration.target.models.Interaction();
        targetInteraction.id = sourceOffset + interaction.id;
        targetInteraction.name = interaction.name;
        targetInteraction.creationDate = interaction.creationDate;
        targetInteraction.businessUnit = businessUnit;
        // Avoid using a remapping Map
        targetInteraction.type = interactionTypeRepository.findByCode(interaction.type.code);
        targetInteraction.documents = documentRepository.findAllById(interaction.documents
                .stream().map(x -> sourceOffset + x.id).collect(Collectors.toList()));

        return targetInteraction;
    }
}
