package com.fgenaudet.batchmigration.processors;

import com.fgenaudet.batchmigration.source.models.InteractionType;
import com.fgenaudet.batchmigration.target.dao.TargetBusinessUnitRepository;
import com.fgenaudet.batchmigration.target.models.BusinessUnit;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class InteractionTypeProcessor implements ItemProcessor<InteractionType, com.fgenaudet.batchmigration.target.models.InteractionType> {
    @Autowired
    private TargetBusinessUnitRepository businessUnitRepository;

    @Value("#{@environment.getProperty('source.businessUnit.id')}")
    private Long businessUnitId;

    @Value("#{@environment.getProperty('source.offset')}")
    private Integer sourceOffset;

    @Override
    public com.fgenaudet.batchmigration.target.models.InteractionType process(InteractionType interactionType) {
        BusinessUnit businessUnit = businessUnitRepository.findById(businessUnitId).orElse(null);

        com.fgenaudet.batchmigration.target.models.InteractionType targetType = new com.fgenaudet.batchmigration.target.models.InteractionType();
        targetType.id = sourceOffset + interactionType.id;
        targetType.name = interactionType.name;
        targetType.code = interactionType.code;
        targetType.businessUnit = businessUnit;

        return targetType;
    }
}
