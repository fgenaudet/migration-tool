package com.fgenaudet.batchmigration.processors;

import com.fgenaudet.batchmigration.source.models.Person;
import com.fgenaudet.batchmigration.target.dao.TargetInteractionRepository;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.util.stream.Collectors;

/**
 * Person entity is not segragated, so no Business Unit is required here
 * TODO Merge values from source & secondSource
 */
@Component
public class PersonProcessor implements ItemProcessor<Person, com.fgenaudet.batchmigration.target.models.Person> {
    @Autowired
    private TargetInteractionRepository interactionRepository;

    @Value("#{@environment.getProperty('source.offset')}")
    private Integer sourceOffset;

    @Override
    public com.fgenaudet.batchmigration.target.models.Person process(Person person) {
        com.fgenaudet.batchmigration.target.models.Person targetPerson = new com.fgenaudet.batchmigration.target.models.Person();
        targetPerson.id = sourceOffset + person.id;
        targetPerson.firstName = person.firstName;
        targetPerson.lastName = person.lastName;
        targetPerson.personInteraction = interactionRepository.findAllById(person.personInteraction.stream()
                .map(x -> sourceOffset + x.id).collect(Collectors.toList()));

        // Because of Cascade ALL
        targetPerson.personInteraction.stream().forEach(i -> i.author = targetPerson);

        return targetPerson;
    }
}
