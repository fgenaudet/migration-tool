package com.fgenaudet.batchmigration.processors;

import com.fgenaudet.batchmigration.secondsource.models.Person;
import com.fgenaudet.batchmigration.target.dao.TargetInteractionRepository;
import com.fgenaudet.batchmigration.target.dao.TargetPersonRepository;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.util.stream.Collectors;

/**
 * Person entity is not segragated, so no Business Unit is required here
 */
@Component
public class PersonSecondProcessor implements ItemProcessor<Person, com.fgenaudet.batchmigration.target.models.Person> {
    @Autowired
    private TargetInteractionRepository interactionRepository;

    @Autowired
    private TargetPersonRepository personRepository;

    @Value("#{@environment.getProperty('secondsource.offset')}")
    private Integer sourceOffset;

    @Override
    public com.fgenaudet.batchmigration.target.models.Person process(Person person) {
        com.fgenaudet.batchmigration.target.models.Person targetPerson = new com.fgenaudet.batchmigration.target.models.Person();

        // Find existing value by first and last name
        com.fgenaudet.batchmigration.target.models.Person existingValue = personRepository.findByFirstNameAndLastName(person.firstName, person.lastName);
        targetPerson.firstName = person.firstName;
        targetPerson.lastName = person.lastName;

        if (existingValue != null) {
            targetPerson.id = existingValue.id;
        } else {
            targetPerson.id = sourceOffset + person.id;
        }

        targetPerson.personInteraction = interactionRepository.findAllById(person.personInteraction.stream()
                .map(x -> sourceOffset + x.id).collect(Collectors.toList()));

        // Because of Cascade ALL
        targetPerson.personInteraction.stream().forEach(i -> i.author = targetPerson);
        return targetPerson;


    }
}
