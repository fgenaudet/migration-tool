package com.fgenaudet.batchmigration.processors;

import com.fgenaudet.batchmigration.secondsource.models.InteractionType;
import com.fgenaudet.batchmigration.target.dao.TargetBusinessUnitRepository;
import com.fgenaudet.batchmigration.target.dao.TargetInteractionTypeRepository;
import com.fgenaudet.batchmigration.target.models.BusinessUnit;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class InteractionTypeSecondProcessor implements ItemProcessor<InteractionType, com.fgenaudet.batchmigration.target.models.InteractionType> {
    @Autowired
    private TargetBusinessUnitRepository businessUnitRepository;

    @Autowired
    private TargetInteractionTypeRepository interactionTypeRepository;

    @Value("#{@environment.getProperty('secondsource.businessUnit.id')}")
    private Long businessUnitId;

    @Value("#{@environment.getProperty('secondsource.offset')}")
    private Integer secondsourceOffset;

    @Value("#{@environment.getProperty('both.businessUnit.id')}")
    private Long allBusinessUnitId;

    @Override
    public com.fgenaudet.batchmigration.target.models.InteractionType process(InteractionType interactionType) {
        BusinessUnit businessUnit = businessUnitRepository.findById(businessUnitId).orElse(null);
        com.fgenaudet.batchmigration.target.models.InteractionType targetType = new com.fgenaudet.batchmigration.target.models.InteractionType();

        // Test already existing value
        com.fgenaudet.batchmigration.target.models.InteractionType typeRepositoryByCode = interactionTypeRepository.findByCode(interactionType.code);
        targetType.name = interactionType.name;
        targetType.code = interactionType.code;

        if (typeRepositoryByCode != null) {
            BusinessUnit allBusinessUnit = businessUnitRepository.findById(allBusinessUnitId).orElse(null);

            targetType.id = typeRepositoryByCode.id;
            targetType.businessUnit = allBusinessUnit;
        } else {
            targetType.id = secondsourceOffset + interactionType.id;
            targetType.businessUnit = businessUnit;
        }

        return targetType;
    }
}
