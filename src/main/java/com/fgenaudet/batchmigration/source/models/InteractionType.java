package com.fgenaudet.batchmigration.source.models;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class InteractionType {
    @Id
    public Long id;

    public String code;
    public String name;
}
