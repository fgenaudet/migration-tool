package com.fgenaudet.batchmigration.source.models;

import javax.persistence.*;
import java.util.List;

@Entity
public class Person {
    @Id
    public Long id;

    public String firstName;
    public String lastName;

    @OneToMany(fetch = FetchType.EAGER, mappedBy = "author")
    public List<Interaction> personInteraction;
}
