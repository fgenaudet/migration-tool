package com.fgenaudet.batchmigration.source.models;

import javax.persistence.Entity;
import javax.persistence.Id;
import java.time.LocalDate;

@Entity
public class Document {
    @Id
    public Long id;

    public String name;
    public LocalDate creationDate;
    public String content;
}
