package com.fgenaudet.batchmigration.source.models;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

@Entity
public class Interaction {
    @Id
    public Long id;

    public String name;

    @Column(name = "creationDate", columnDefinition = "DATE")
    public Date creationDate;

    @ManyToOne(fetch = FetchType.EAGER)
    public InteractionType type;

    @ManyToOne(fetch = FetchType.EAGER)
    public Person author;

    @ManyToMany(fetch = FetchType.EAGER)
    public List<Document> documents;
}
