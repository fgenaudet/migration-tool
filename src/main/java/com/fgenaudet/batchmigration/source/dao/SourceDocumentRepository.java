package com.fgenaudet.batchmigration.source.dao;

import com.fgenaudet.batchmigration.source.models.Document;
import org.springframework.data.jpa.repository.JpaRepository;

public interface SourceDocumentRepository extends JpaRepository<Document, Long> {
}
