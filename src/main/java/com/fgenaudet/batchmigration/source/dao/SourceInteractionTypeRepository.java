package com.fgenaudet.batchmigration.source.dao;

import com.fgenaudet.batchmigration.source.models.InteractionType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface SourceInteractionTypeRepository extends JpaRepository<InteractionType, Long> {
}
