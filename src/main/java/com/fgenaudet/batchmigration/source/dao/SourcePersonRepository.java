package com.fgenaudet.batchmigration.source.dao;

import com.fgenaudet.batchmigration.source.models.Person;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface SourcePersonRepository extends JpaRepository<Person, Long> {
}
