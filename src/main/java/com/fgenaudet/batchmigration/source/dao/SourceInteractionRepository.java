package com.fgenaudet.batchmigration.source.dao;

import com.fgenaudet.batchmigration.source.models.Interaction;
import org.springframework.data.jpa.repository.JpaRepository;

public interface SourceInteractionRepository extends JpaRepository<Interaction, Long> {
}
