package com.fgenaudet.batchmigration.tests;

import com.fgenaudet.batchmigration.secondsource.dao.SecondSourceDocumentRepository;
import com.fgenaudet.batchmigration.source.dao.SourceDocumentRepository;
import com.fgenaudet.batchmigration.source.models.Document;
import com.fgenaudet.batchmigration.target.dao.TargetDocumentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.ExampleMatcher;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class EnsureDocumentMigrationConsistency {
    @Autowired
    private SourceDocumentRepository sourceDocumentRepository;

    @Autowired
    private SecondSourceDocumentRepository secondSourceDocumentRepository;

    @Autowired
    private TargetDocumentRepository targetDocumentRepository;

    public void run() {
        checkSource();
        checkSecondSource();
    }

    private void checkSecondSource() {
        List<com.fgenaudet.batchmigration.secondsource.models.Document> documents = secondSourceDocumentRepository.findAll();
        for (com.fgenaudet.batchmigration.secondsource.models.Document document : documents) {
            Example<com.fgenaudet.batchmigration.target.models.Document> example = toExample(document);
            com.fgenaudet.batchmigration.target.models.Document targetDocument = targetDocumentRepository.findOne(example).orElse(null);
            if (targetDocument == null) {
                System.err.println(String.format("Migrated document not found: %s,%s", document.id, document.name));
            } else {
                System.out.println(String.format("Migrated document OK: %s,%s ==> %s,%s,%s", document.id, document.name, targetDocument.id, targetDocument.name, targetDocument.businessUnit.code));
            }
        }
    }

    private void checkSource() {
        List<Document> documents = sourceDocumentRepository.findAll();
        for (Document document : documents) {
            Example<com.fgenaudet.batchmigration.target.models.Document> example = toExample(document);
            com.fgenaudet.batchmigration.target.models.Document targetDocument = targetDocumentRepository.findOne(example).orElse(null);
            if (targetDocument == null) {
                System.err.println(String.format("Migrated document not found: %s,%s", document.id, document.name));
            } else {
                System.out.println(String.format("Migrated document OK: %s,%s ==> %s,%s,%s", document.id, document.name, targetDocument.id, targetDocument.name, targetDocument.businessUnit.code));
            }
        }
    }

    private Example<com.fgenaudet.batchmigration.target.models.Document> toExample(Document document) {
        com.fgenaudet.batchmigration.target.models.Document query = new com.fgenaudet.batchmigration.target.models.Document();
        query.creationDate = document.creationDate;
        query.content = document.content;
        query.name = document.name;

        ExampleMatcher ignoringExampleMatcher = ExampleMatcher.matchingAll()
                .withIgnorePaths("id", "businessUnit");

        return Example.of(query, ignoringExampleMatcher);
    }

    private Example<com.fgenaudet.batchmigration.target.models.Document> toExample(com.fgenaudet.batchmigration.secondsource.models.Document document) {
        com.fgenaudet.batchmigration.target.models.Document query = new com.fgenaudet.batchmigration.target.models.Document();
        query.creationDate = document.creationDate;
        query.content = document.content;
        query.name = document.name;

        ExampleMatcher ignoringExampleMatcher = ExampleMatcher.matchingAll()
                .withIgnorePaths("id", "businessUnit");

        return Example.of(query, ignoringExampleMatcher);
    }
}
