package com.fgenaudet.batchmigration.tests;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class EnsureMigrationConsistency {
    @Autowired
    private EnsureTypeMigrationConsistency ensureTypeMigrationConsistency;

    @Autowired
    private EnsureDocumentMigrationConsistency ensureDocumentMigrationConsistency;

    @Autowired
    private EnsureInteractionMigrationConsistency ensureInteractionMigrationConsistency;

    public void run() {
        ensureTypeMigrationConsistency.run();
        ensureDocumentMigrationConsistency.run();
        ensureInteractionMigrationConsistency.run();
    }
}
