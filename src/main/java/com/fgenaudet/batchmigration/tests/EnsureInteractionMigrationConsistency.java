package com.fgenaudet.batchmigration.tests;

import com.fgenaudet.batchmigration.secondsource.dao.SecondSourceInteractionRepository;
import com.fgenaudet.batchmigration.source.dao.SourceInteractionRepository;
import com.fgenaudet.batchmigration.source.models.Interaction;
import com.fgenaudet.batchmigration.target.dao.TargetInteractionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.ExampleMatcher;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class EnsureInteractionMigrationConsistency {
    @Autowired
    private SourceInteractionRepository sourceInteractionRepository;

    @Autowired
    private SecondSourceInteractionRepository secondSourceInteractionRepository;

    @Autowired
    private TargetInteractionRepository targetInteractionRepository;

    public void run() {
        checkSource();
        checkSecondSource();
    }

    private void checkSecondSource() {
        List<com.fgenaudet.batchmigration.secondsource.models.Interaction> interactions = secondSourceInteractionRepository.findAll();
        for (com.fgenaudet.batchmigration.secondsource.models.Interaction interaction : interactions) {
            Example<com.fgenaudet.batchmigration.target.models.Interaction> example = toExample(interaction);
            com.fgenaudet.batchmigration.target.models.Interaction targetInteraction = targetInteractionRepository.findOne(example).orElse(null);

            if (targetInteraction == null) {
                System.err.println(String.format("Migrated Interaction not found : %s,%s", interaction.id, interaction.name));
            } else {
                System.out.println(String.format("Migrated Interaction OK %s,%s ==> %s,%s,%s", interaction.id, interaction.name,
                        targetInteraction.id, targetInteraction.name, targetInteraction.businessUnit.code));
            }
        }
    }

    private void checkSource() {
        List<Interaction> interactions = sourceInteractionRepository.findAll();
        for (Interaction interaction : interactions) {
            Example<com.fgenaudet.batchmigration.target.models.Interaction> example = toExample(interaction);
            com.fgenaudet.batchmigration.target.models.Interaction targetInteraction = targetInteractionRepository.findOne(example).orElse(null);

            if (targetInteraction == null) {
                System.err.println(String.format("Migrated Interaction not found : %s,%s", interaction.id, interaction.name));
            } else {
                System.out.println(String.format("Migrated Interaction OK %s,%s ==> %s,%s,%s", interaction.id, interaction.name,
                        targetInteraction.id, targetInteraction.name, targetInteraction.businessUnit.code));
            }
        }
    }

    private Example<com.fgenaudet.batchmigration.target.models.Interaction> toExample(Interaction interaction) {
        ExampleMatcher ignoringExampleMatcher = ExampleMatcher.matchingAll()
                .withIgnorePaths("id", "businessUnit", "documents", "author", "type");
        com.fgenaudet.batchmigration.target.models.Interaction query = new com.fgenaudet.batchmigration.target.models.Interaction();
        query.creationDate = interaction.creationDate;
        query.name = interaction.name;

        return Example.of(query, ignoringExampleMatcher);
    }

    private Example<com.fgenaudet.batchmigration.target.models.Interaction> toExample(com.fgenaudet.batchmigration.secondsource.models.Interaction interaction) {
        ExampleMatcher ignoringExampleMatcher = ExampleMatcher.matchingAll()
                .withIgnorePaths("id", "businessUnit", "documents", "author", "type");
        com.fgenaudet.batchmigration.target.models.Interaction query = new com.fgenaudet.batchmigration.target.models.Interaction();
        query.creationDate = interaction.creationDate;
        query.name = interaction.name;

        return Example.of(query, ignoringExampleMatcher);
    }

}
