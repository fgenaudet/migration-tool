package com.fgenaudet.batchmigration.tests;

import com.fgenaudet.batchmigration.secondsource.dao.SecondSourceInteractionTypeRepository;
import com.fgenaudet.batchmigration.source.dao.SourceInteractionTypeRepository;
import com.fgenaudet.batchmigration.source.models.InteractionType;
import com.fgenaudet.batchmigration.target.dao.TargetInteractionTypeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class EnsureTypeMigrationConsistency {
    @Autowired
    private SourceInteractionTypeRepository sourceInteractionTypeRepository;

    @Autowired
    private SecondSourceInteractionTypeRepository secondSourceInteractionTypeRepository;

    @Autowired
    private TargetInteractionTypeRepository targetInteractionTypeRepository;

    // TODO Log this into file so it's easier to debug later
    public void run() {
        checkSource();
        checkSecondSource();
    }

    private void checkSource() {
        List<InteractionType> allSource = sourceInteractionTypeRepository.findAll();
        for (InteractionType type : allSource) {
            com.fgenaudet.batchmigration.target.models.InteractionType migratedType = targetInteractionTypeRepository.findByCode(type.code);
            if (migratedType == null) {
                System.err.println(String.format("Migrated Type not found : %s,%s", type.id, type.code));
            } else {
                System.out.println(String.format("Migrated Type OK : %s,%s ==> %s,%s,%s", type.id, type.code, migratedType.id, migratedType.code, migratedType.businessUnit.code));
            }
        }
    }

    private void checkSecondSource() {
        List<com.fgenaudet.batchmigration.secondsource.models.InteractionType> allSource = secondSourceInteractionTypeRepository.findAll();
        for (com.fgenaudet.batchmigration.secondsource.models.InteractionType type : allSource) {
            com.fgenaudet.batchmigration.target.models.InteractionType migratedType = targetInteractionTypeRepository.findByCode(type.code);
            if (migratedType == null) {
                System.err.println(String.format("Migrated Type not found : %s,%s", type.id, type.code));
            } else {
                System.out.println(String.format("Migrated Type OK : %s,%s ==> %s,%s,%s", type.id, type.code, migratedType.id, migratedType.code, migratedType.businessUnit.code));
            }
        }
    }
}
