package com.fgenaudet.batchmigration.utils;

import com.fgenaudet.batchmigration.source.dao.SourceDocumentRepository;
import com.fgenaudet.batchmigration.source.dao.SourceInteractionRepository;
import com.fgenaudet.batchmigration.source.dao.SourceInteractionTypeRepository;
import com.fgenaudet.batchmigration.source.dao.SourcePersonRepository;
import com.fgenaudet.batchmigration.source.models.Document;
import com.fgenaudet.batchmigration.source.models.Interaction;
import com.fgenaudet.batchmigration.source.models.InteractionType;
import com.fgenaudet.batchmigration.source.models.Person;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.util.Arrays;
import java.util.Date;

@Component
public class InitSourceData {
    @Autowired
    private SourceInteractionTypeRepository sourceInteractionTypeRepository;

    @Autowired
    private SourceDocumentRepository sourceDocumentRepository;

    @Autowired
    private SourceInteractionRepository sourceInteractionRepository;

    @Autowired
    private SourcePersonRepository sourcePersonRepository;

    @Transactional("sourceTransactionManager")
    public void run() {
        generateInteractionTypes();
        generateDocuments();
        generatePersons();
        generateInteractions();
    }

    private void generateInteractions() {
        sourceInteractionRepository.deleteAll();
        sourceInteractionRepository.flush();

        for (long i = 0; i < 10; i++) {
            sourceInteractionRepository.saveAndFlush(interaction(i, i, 0L, i, 9L));
        }
    }

    private void generateInteractionTypes() {
        sourceInteractionTypeRepository.deleteAll();
        sourceInteractionTypeRepository.flush();

        for (long i = 0; i < 10; i++) {
            sourceInteractionTypeRepository.saveAndFlush(interactionType(i));
        }
    }

    private void generateDocuments() {
        sourceDocumentRepository.deleteAll();
        sourceDocumentRepository.flush();

        for (long i = 0; i < 10; i++) {
            sourceDocumentRepository.saveAndFlush(document(i));
        }
    }

    public void generatePersons() {
        sourcePersonRepository.deleteAll();
        sourcePersonRepository.flush();

        for (long i = 0; i < 10; i++) {
            sourcePersonRepository.saveAndFlush(person(i));
        }
    }

    private Person person(Long id) {
        Person person = new Person();
        person.id = id;
        person.firstName = "First Name " + id;
        person.lastName = "Last Name " + id;

        return person;
    }

    private Interaction interaction(Long id, Long itId, Long... docIds) {
        Interaction interaction = new Interaction();
        interaction.id = id;
        interaction.creationDate = new Date();
        interaction.name = "Interaction " + id;
        interaction.type = sourceInteractionTypeRepository.getOne(itId);
        interaction.documents = sourceDocumentRepository.findAllById(Arrays.asList(docIds));
        interaction.author = sourcePersonRepository.getOne(id);

        return interaction;
    }

    private InteractionType interactionType(Long id) {
        InteractionType interactionType = new InteractionType();
        interactionType.id = id;
        interactionType.name = "Interaction Type " + id;
        interactionType.code = "IT-" + id;

        return interactionType;
    }

    private Document document(Long id) {
        Document document = new Document();
        document.id = id;
        document.name = "Document " + id;
        document.creationDate = LocalDate.now();
        document.content = "01010101010100001010101010101010100101";
        return document;
    }
}
