package com.fgenaudet.batchmigration.utils;

import com.fgenaudet.batchmigration.target.dao.*;
import com.fgenaudet.batchmigration.target.models.BusinessUnit;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

@Component
public class InitTargetData {
    @Autowired
    private TargetBusinessUnitRepository targetBusinessUnitRepository;

    @Autowired
    private TargetInteractionTypeRepository targetInteractionTypeRepository;

    @Autowired
    private TargetDocumentRepository targetDocumentRepository;

    @Autowired
    private TargetInteractionRepository targetInteractionRepository;

    @Autowired
    private TargetPersonRepository targetPersonRepository;

    @Transactional("targetTransactionManager")
    public void run() {
        clearAll();
        generateBusinessUnit();
    }

    public void clearAll() {
        targetDocumentRepository.deleteAll();
        targetInteractionTypeRepository.deleteAll();
        targetInteractionRepository.deleteAll();
        targetPersonRepository.deleteAll();
    }



    private void generateBusinessUnit() {
        targetBusinessUnitRepository.deleteAll();
        targetBusinessUnitRepository.flush();

        for (long i = 0; i < 3; i++) {
            targetBusinessUnitRepository.saveAndFlush(businessUnit(i));
        }
    }

    private BusinessUnit businessUnit(Long id) {
        BusinessUnit bu = new BusinessUnit();
        bu.id = id;
        bu.code = "BU-" + id;
        bu.name = "Business Unit " + id;

        return bu;
    }
}
