package com.fgenaudet.batchmigration.utils;

import com.fgenaudet.batchmigration.secondsource.dao.SecondSourceDocumentRepository;
import com.fgenaudet.batchmigration.secondsource.dao.SecondSourceInteractionRepository;
import com.fgenaudet.batchmigration.secondsource.dao.SecondSourceInteractionTypeRepository;
import com.fgenaudet.batchmigration.secondsource.dao.SecondSourcePersonRepository;
import com.fgenaudet.batchmigration.secondsource.models.Document;
import com.fgenaudet.batchmigration.secondsource.models.Interaction;
import com.fgenaudet.batchmigration.secondsource.models.InteractionType;
import com.fgenaudet.batchmigration.secondsource.models.Person;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.util.Arrays;
import java.util.Date;

@Component
public class InitSecondsourceData {
    @Autowired
    private SecondSourceInteractionTypeRepository secondSourceInteractionTypeRepository;

    @Autowired
    private SecondSourceDocumentRepository secondSourceDocumentRepository;

    @Autowired
    private SecondSourcePersonRepository secondSourcePersonRepository;

    @Autowired
    private SecondSourceInteractionRepository secondsourceInteractionRepository;

    @Transactional("secondsourceTransactionManager")
    public void run() {
        generateInteractionTypes();
        generateDocuments();
        generatePersons();
        generateInteractions();
    }

    private void generateDocuments() {
        secondSourceDocumentRepository.deleteAll();
        secondSourceDocumentRepository.flush();

        for (long i = 0; i < 15; i++) {
            secondSourceDocumentRepository.saveAndFlush(document(i));
        }
    }

    private void generateInteractionTypes() {
        secondSourceInteractionTypeRepository.deleteAll();
        secondSourceInteractionTypeRepository.flush();

        for (long i = 0; i < 15; i++) {
            secondSourceInteractionTypeRepository.saveAndFlush(interactionType(i));
        }
    }

    public void generatePersons() {
        secondSourcePersonRepository.deleteAll();
        secondSourcePersonRepository.flush();

        for (long i = 0; i < 15; i++) {
            secondSourcePersonRepository.saveAndFlush(person(i));
        }
    }

    private void generateInteractions() {
        secondsourceInteractionRepository.deleteAll();
        secondsourceInteractionRepository.flush();

        for (long i = 0; i < 15; i++) {
            secondsourceInteractionRepository.saveAndFlush(interaction(i, i, 0L, i, 9L));
        }
    }

    private Document document(Long id) {
        Document document = new Document();
        document.id = id;
        document.name = "Document " + id;
        document.creationDate = LocalDate.now();
        document.content = "02020202020200002020202020202020200202";
        document.unusedField = "DO NOT MIGRATE ME";

        return document;
    }

    private InteractionType interactionType(Long id) {
        InteractionType interactionType = new InteractionType();
        interactionType.id = id;
        interactionType.name = "Interaction Type " + id;

        // Generate 5 identical code value w/ source
        interactionType.code = "IT-" + (id + 5);

        return interactionType;
    }

    private Person person(Long id) {
        Person person = new Person();
        person.id = id + 3;

        // Generate 5 identical code value w/ source
        person.firstName = "First Name " + (id + 5);
        person.lastName = "Last Name " + (id + 5);

        return person;
    }

    private Interaction interaction(Long id, Long itId, Long... docIds) {
        Interaction interaction = new Interaction();
        interaction.id = id;
        interaction.creationDate = new Date();
        interaction.name = "Interaction Second" + id;
        interaction.type = secondSourceInteractionTypeRepository.getOne(itId);
        interaction.documents = secondSourceDocumentRepository.findAllById(Arrays.asList(docIds));
        interaction.author = secondSourcePersonRepository.getOne(id + 3);

        return interaction;
    }
}
