### DB Migration project in Java
What is covered :
- 3 databases
    - 2 sources DB (source & second source)
    - 1 target DB (that is the merge from the last two)

- Migrations cases
    - data from source have an offset (100) when migrated to target
    - data from second source have an offset (200) when migrated to target
    - Interactions & Documents do not have overlapping btw source / secondsource
    - Interaction Types & Persons do have overlapping btw source / secondsource
        - But id translation map is not required since I'm using dao to fetch by code
    - Document from second source do have additional, not migrated field
    - Person are migrated after Interaction
        - An additional field (Set<Interaction>) is added to target entity
        - With cascade.all
        - That allow me to recover these relations after having migrated Interactions 


### Install / Start DB (without docker-compose)
sudo docker run --name mysql -e MYSQL_ROOT_PASSWORD=root -d mysql:latest

### Check logs (without docker-compose)
sudo docker logs mysql

### Run mysql commands (without docker-compose)
sudo docker exec -it mysql mysql -uroot -p

### Start project (with docker-compose)
sudo docker-compose up -d

==> Then run java project

### Create databases
#### in db1 (3306)
CREATE DATABASE db_source;
#### in db2 (3307)
CREATE DATABASE db_second_source;
#### in db3 (4306)
CREATE DATABASE db_target;

### TODOS
 - File logger
 


